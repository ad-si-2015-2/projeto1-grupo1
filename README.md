**Regras do Jogo**

Trata-se de um jogo de cartas que obedece as seguintes regras:

Cada jogador recebe três cartas na rodada inicial, e essas cartas cada carta
contendo um número. O primeiro jogador que obtiver três cartas de valor 
igual vence o jogo. Os outros jogadores devem bater assim que um dos 
jogadores vencer, o último a bater perde o jogo.

O jogo possuirá as seguintes etapas:

* As cartas são distribuídas, três para cada jogador na primeira rodada.

* O primeiro jogador puxa uma cara do baralho e decide se quer ou não aquela carta, se ele quiser, ele substitui uma das suas cartas por
esta, e a carta substituída vira o descarte para o próximo jogador. Se ele não quiser, a carta comprada vira o descarte.

* O jogador seguinte analisa se quer ou não o descarte. Caso queira, substitui uma de suas cartas e descarta outra, caso não, puxa uma carta
do baralho e repete o procedimento anterior.

* Isso ocorrerá até que um dos jogadores ganham e todos batam.

O protocolo do jogo será:

* Digitar /iniciar para iniciar o jogo.

* Digitar /listar para listar jogadores.

* Digitar /acc para aceitar descarte ou nova carta.

* Digitar /bater para bater quando um dos jogadores ganhar.

Observação: apertar enter, passa jogada ou dispensa carta. Comandos aleatórios não serão reconhecidos e serão tratados como enter.

**INSTRUÇÕES PARA EXECUÇÃO DA APLICAÇÃO:**

* Executar a classe ServidorJogo.java

* Conectar-se com via telnet na porta 6789 (primeira conexão)

* Digitar nome do jogador

* Digitar quantidade de jogadores (só o primeiro jogador deve iniciar o jogo)

* Conectar-se via telnet com a quantidade restante de jogadores

* Digitar /iniciar com algum dos jogadores após a partida completa

