package jogoCopoDagua;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import jogoCopoDagua.Baralho;

public class ServidorJogo {

    ArrayList<ServidorThread> servidores
            = new ArrayList<ServidorThread>();       //variavel que armazena a instancia de cada servidor que gerencia cada cliente
    int porta = 6789;       //porta de conexao
    int status = 0;         //variável que controla o momento do jogo (iniciação ou execução ou finalização) - 0 => iniciação
    int quantidadeJogadores = 0;            //variável que controla a quantidade de jogadores
    int jogadorDaVez = 1;           // variável que controla a vez de cada jogador
    int descarte = -1;  //variavel que ira armazenar o descarte da rodada
    int controleUltimoBatedor = 1;  //variavel que controla quantos jogadores ja bateram ao final do jogo
    Baralho baralho;        //objeto baralho que será utilizado pro todos os clientes
    ServerSocket conexaoServidor;   //variavel que armazena a conexao com o servidor central

    /* Construtor da Classe inicializando ServerSocket
     * 
     * 
     */
    public ServidorJogo(int porta) {
        try {
            this.conexaoServidor = new ServerSocket(porta); // esperando conexoes externas (telnet ou app cliente)
            System.out.println("Esperando conexoes na porta " + porta);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Método que retorna a conexao com o servidor

    ServerSocket getConexaoServidor() {
        return conexaoServidor;
    }

    // Método que retorna todos os servidores ativos (clientes)

    ArrayList<ServidorThread> getServidores() {
        return servidores;
    }

    //Método que aciona nova carta do baralho solicitada pelo cliente
    int getNovaCarta() {
        int carta = baralho.novaCarta(); // puxando nova carta
        return carta;
    }

    //Método que verifica se o jogador venceu na rodada
    boolean verificaVencedor(List<Integer> cartasParticipante) {
        int referencia = cartasParticipante.get(0);  //pega a primeira carta     
        int contadorCartas = 0;
        for (int carta : cartasParticipante) {// verifica quantas cartas sao iguais a primeira
            if (referencia == carta) {
                contadorCartas++;
            }
        }
        // as três cartas são iguais? contador = 3
        if (contadorCartas == 3) {
            status = 2; // um jogador ganhou
            return true;
        } else {
            return false;
        }
    }

    // Método que retorna status do jogo

    int getStatus() {
        return status;
    }

    //Método que retorna quantitade de jogadores

    int getQuantidadeJogadores() {
        return quantidadeJogadores;
    }

    //Método que atualiza a quantidade de jogadores

    void setQuantidadeJogadores(int quantidadeJogadores) {
        this.quantidadeJogadores = quantidadeJogadores;
    }

    // Método que retorna que jogador deve jogar (controle do jogo)

    int getJogadorDaVez() {
        return jogadorDaVez;
    }

    // Método que atualiza qual jogador deve jogar (controle do jogo)

    void setJogadorDaVez(int jogadorDaVez) {
        this.jogadorDaVez = jogadorDaVez;
    }

    // Método que incrementa o jogador da vez para que o próximo possa jogar (controle do jogo)

    void incrementarPosicao() {
        jogadorDaVez++;
    }

    // Método que incrementa quantos jogadores bateram (controle do jogo)
    void incrementaBatedor() {
        controleUltimoBatedor++;
    }

    // Método que retorna quantos jogadores ja bateram (controle do jogo)

    int getControleUltimoBatedor() {
        return controleUltimoBatedor;
    }

    // Método que retorna a carta descartada

    int getDescarte() {
        return descarte;
    }

    // Método que atualiza a carta descartada

    void setDescarte(int descarte) {
        this.descarte = descarte;
    }

    //Método para notificar vez a todos os jogadores
    void notificarVezTodosJogadores() {
        //notificando jogador da vez
        for (ServidorThread servidor : servidores) {
            servidor.notificar("Vez do jogador " + jogadorDaVez + ". \n");
        }
    }

    //Método para notificar o vencedor do jogo a todos os jogadores
    void notificarVencedorJogadores() {
        //notificando jogador da vez
        for (ServidorThread servidor : servidores) {
            servidor.notificar("O jogador " + jogadorDaVez + " venceu o jogo. Aperte enter para finalizar! \n");
        }
    }

    // Método para distribuir cartas (início do jogo)

    List<Integer> distribuirCartas() {
        List<Integer> cartasIniciais = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            cartasIniciais.add(this.getNovaCarta());//entregando três cartas por jogador
        }
        return cartasIniciais;
    }

    // Método que inicia a partida

    void iniciar() {
        status = 1; // setando o status para "execução"
        baralho = new Baralho(); // criando baralho
        baralho.criarBaralho(); // preenchendo baralho
        for (ServidorThread servidor : servidores) { //notificando todos os servidores que o jogo começou
            servidor.notificar("O jogo comecou!\n");
            servidor.notificar("Voce e o jogador " + servidor.posicao + ".\n");//informando posição que cada jogador recebeu na partida (ordem do jogo)
        }
    }

    // Método que finaliza a partida

    void finalizar() {
        status = -1; // setando status para finalizar jogo
        for (ServidorThread servidor : servidores) {// notificando vencedor e que o jogo terminou a todos os jogadores
            servidor.notificar("O jogo terminou!\n");
            servidor.notificar("O jogador " + this.jogadorDaVez + " venceu!\n");
        }
    }

    //Método main (execução do servidor)

    public static void main(String argv[]) throws Exception {
        ServidorJogo servidorAMU = new ServidorJogo(6789);// esperando conexoes

        while (true) {//ficar rodando infinitamente
            if (servidorAMU.getStatus() == 0) {
                Socket conexao = servidorAMU.getConexaoServidor().accept(); // aceitando conexao
                ServidorThread st = new ServidorThread(conexao, servidorAMU);// instanciando novo objeto SeridorThread para gerenciar cliente
                servidorAMU.getServidores().add(st); // adicionando na lista de servidor ativos
                st.start(); // startando servidor thread
            }
        }
    }
}
