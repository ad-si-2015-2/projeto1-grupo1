package jogoCopoDagua;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class ServidorThread extends Thread {

    Socket conexao;     //variável de conexão com o socket 
    String nome;        //variável que armazena nome do jogador
    int batedor = 0;    //vairável que armazena a posição que o jogador bateu
    int posicao = 0;        //variável que controla a posição do jogador dentro do jogo (ordem de jogadas)
    List<Integer> cartasJogador;        //armazena as cartas do jogador
    ServidorJogo servidorJogo;      //instância do servidor central
    BufferedReader doCliente;       //Variável que armazena comandos digitados pelo cliente
    DataOutputStream paraCliente;   //Variável que armazena dados que o cliente recebe

    //Método construtor ServidorThread
    public ServidorThread(Socket conexao, ServidorJogo servidorJogo) {
        try {
            //inicializando variáveis
            this.conexao = conexao;
            this.servidorJogo = servidorJogo;
            //Inicializando variável de entrada de dados do cliente
            doCliente = new BufferedReader(
                    new InputStreamReader(
                            conexao.getInputStream()
                    )
            );
            //inicializando variável de saída de dados para o cliente
            paraCliente = new DataOutputStream(
                    conexao.getOutputStream()
            );
        } catch (Exception e) {//tratando exceções de conexao
            e.printStackTrace();
        }
    }

    //Método que manda notificação para jogador
    public void notificar(String mensagem) {
        try {
            paraCliente.writeBytes(mensagem);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    //Método para imprimir todas as cartas do jogador
    public void imprimirCartasJogador(List<Integer> cartas) {
        try {
            int contadorCartas = 1;//Variável de controla o índice da carta
            for (int carta : cartasJogador) {
                paraCliente.writeBytes("Carta " + contadorCartas + ": " + carta + "\n");//imprimindo carta para o cliente
                contadorCartas++; // incrementando
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Método que mantém a thread ativa recebendo comandos do cliente

    @Override
    public void run() {
        try {
            //Salvando nome do jogador
            paraCliente.writeBytes("Forneca seu nome: ");
            nome = doCliente.readLine();
            while("".equals(nome.trim())){
                paraCliente.writeBytes("Insira nome valido!");
                paraCliente.writeBytes("Forneca seu nome: ");
                nome = doCliente.readLine();
            }
 
            //Verificando status do jogo
            while (servidorJogo.getStatus() == 0) {//status = iniciação
                if (servidorJogo.getQuantidadeJogadores() == 0) {//O primeiro jogador deve escolher a quantidade de jogadores
                    if (servidorJogo.getJogadorDaVez() == 1) { // caso seja o primeiro jogador
                        //Salvando a quantidade de jogadores da partida
                     
                        int quantidade = 0 ;
                        while(quantidade <= 1 ){
                            try{
                                paraCliente.writeBytes("Digite a quantidade de jogadores desta partida:\n");
                                quantidade = Integer.parseInt(doCliente.readLine());
                            }
                            catch(NumberFormatException e){
                                paraCliente.writeBytes("Insira um valor valido!\n");
                                quantidade = 0;
                            }
                        }    
                        //Caso o numero de jogadores seja maior que 5
                        if (quantidade > 5) {
                            paraCliente.writeBytes("A quantidade maxima de jogadores e 5. Esta partida tera 5 jogadores.\n"); //informar cliente
                            servidorJogo.setQuantidadeJogadores(5);//atribuir 5 como numero de jogadores da partida
                        } else {
                            servidorJogo.setQuantidadeJogadores(quantidade);//atribuindo numero de jogadores que cliente escolheu
                        }
                     //   this.posicao = servidorJogo.getJogadorDaVez();//atribuindo posicao 1 ao primeiro jogador
                    }

                } else {
                    if (this.posicao == 0) {//caso o jogador ainda não tenha posição
                        servidorJogo.incrementarPosicao();//incrementando posicao
                        this.posicao = servidorJogo.getJogadorDaVez();//setando ordem do jogador no jogo
                    }

                }
                
                //mostrando protocolo ao jogador para iniciar ou listar jogadores
                paraCliente.writeBytes("O jogo ainda nao comecou. "
                        + "Digite '/iniciar' para comecar ou "
                        + "'/listar' para listar os jogadores\n");
                //lendo respsota do cliente
                String comando = doCliente.readLine();

                if (comando.equals("/listar")) { // caso o jogador queira listar os jogadores
                    ArrayList<ServidorThread> servidores
                            = servidorJogo.getServidores(); // preenchendo lista de servidores ativos (um para cada cliente)
                    for (ServidorThread servidorThread : servidores) { // imprimindo nome de cada cliente e sua ordem no jogo
                        paraCliente.writeBytes(servidorThread.nome + ", Ordem no jogo: " + servidorThread.posicao +"\n");
                    }
                } else { // caso o jogador queira iniciar o jogo
                    if (servidorJogo.getServidores().size() < servidorJogo.getQuantidadeJogadores()) {
                        //caso ainda nao haja a quantidade de jogadores pra completar a partida
                        paraCliente.writeBytes("A partida ainda nao esta completa.....AGUARDE.\n"); //informar para o cliente aguardar
                    } else {
                        if (servidorJogo.getStatus() == 0) {//caso seja o primeiro cliente a solicitar inicio
                            //o primeiro jogador que digitar /iniciar com a partida completa começa e o proximo será o com ordem posterior a ele
                            servidorJogo.setJogadorDaVez(posicao);// setando jogador da vez
                            servidorJogo.iniciar();// iniciando jogo
                        }
                    }
                }
            }

            cartasJogador = new ArrayList<>(); // array de cartas de cada jogador
            int novaCarta; //variável que irá receber a nova carta
            while (servidorJogo.getStatus() == 1) {//status = execução (jogo)
                System.out.println("posicao: " + this.posicao);
                System.out.println("Jogador da vez: " + servidorJogo.getJogadorDaVez());
                //verificando se a vez é do jogador que solicita o comando
                if (this.posicao == servidorJogo.getJogadorDaVez()) {
                    //verificando se o jogador ainda não tem cartas
                    if (cartasJogador.isEmpty()) {//caso não tenha
                        boolean verifica = true; //variável para garantir que nenhum jogador ganhe uma mao vencedora
                        cartasJogador = servidorJogo.distribuirCartas(); // solicitando distribuição de cartas
                        while (verifica) {
                            if (servidorJogo.verificaVencedor(cartasJogador)) {//caso a mao seja vencedora, redistribuir
                                cartasJogador = servidorJogo.distribuirCartas(); // solicitando distribuição de cartas
                            } else {
                                verifica = false; // sair do while
                            }
                        }
                        paraCliente.writeBytes("Cartas distribuidas\n");// informar clientes
                        this.imprimirCartasJogador(cartasJogador); // imprimindo cartas jogador
                    }

                    int indiceRemovido;//variável para armazenar indice que deseja-se descartar 
                    String resposta = "";//variável para armazenar a resposta do cliente
                    if (servidorJogo.getDescarte() != -1) {//início do jogo, se ainda nao houve descarte, descarte = -1, não entra no if
                        //informando descarte
                        paraCliente.writeBytes("O descarte do ultimo jogador e: " + servidorJogo.getDescarte() + ". Caso voce aceite o descarte digite /acc. Aperte enter para solicitar uma nova carta.\n");
                        resposta = doCliente.readLine();//lendo resposta
                        if (resposta.equals("/acc")) {//caso o participante aceite o descarte
                            paraCliente.writeBytes("Qual carta voce deseja substituir? Digite o indice da carta\n");
                            //imprimindo cartas
                            this.imprimirCartasJogador(cartasJogador);
                            //lendo indice carta que o jogador deseja descartar
                            indiceRemovido = Integer.parseInt(doCliente.readLine());//armazenando indice removido
                            cartasJogador.add(servidorJogo.getDescarte()); //adicionando nova carta
                            servidorJogo.setDescarte(cartasJogador.get(indiceRemovido - 1));
                            cartasJogador.remove(indiceRemovido - 1); // removendo indice da mao do jogador
                            paraCliente.writeBytes("Suas cartas no momento sao:\n");

                            //imprimindo cartas
                            this.imprimirCartasJogador(cartasJogador);
                        }
                    }
                    if (!resposta.equals("/acc")) {//caso o cliente nao tenha acessado o descarte e so tenha apertado enter
                        //Protocolo para puxar nova carta
                        paraCliente.writeBytes("Digite /nova para puxar uma nova carta. Aperte enter para passar sua vez.\n");
                        resposta = doCliente.readLine();//lendo resposta do cliente
                        if (resposta.equals("/nova")) {//casdo ele aceite puxar a nova carta
                            //puxando nova carta
                            novaCarta = servidorJogo.getNovaCarta();
                            //protocolo para aceitar nova carta
                            paraCliente.writeBytes("A carta sorteada e: " + novaCarta + ". Caso voce aceite digite /acc. Aperte enter para descartar.\n");
                            resposta = doCliente.readLine();//lendo resposta do cliente
                            if (resposta.equals("/acc")) {//caso o participante aceite
                                if (cartasJogador.size() == 3) { // caso já existam 3 cartas - substituir (SEMPRE DEVEM HAVER TRÊS CARTAS)
                                    paraCliente.writeBytes("Qual carta voce deseja substituir? Digite o indice da carta\n");
                                    //imprimindo cartas
                                    this.imprimirCartasJogador(cartasJogador);
                                    indiceRemovido = Integer.parseInt(doCliente.readLine());//armazenando indice removido
                                    servidorJogo.setDescarte(cartasJogador.get(indiceRemovido - 1)); // setando descarte para indice que será removido
                                    cartasJogador.remove(indiceRemovido - 1); // removendo indice da mao do jogador
                                    cartasJogador.add(novaCarta); //adicionando nova carta
                                }
                            } else {
                                servidorJogo.setDescarte(novaCarta);
                            }
                            paraCliente.writeBytes("Suas cartas no momento sao:\n");

                            //imprimindo cartas
                            this.imprimirCartasJogador(cartasJogador);
                        }
                    }
                    //verificando se jogador ganhou
                    if (servidorJogo.verificaVencedor(cartasJogador)) {
                        //caso tenha ganhado
                        paraCliente.writeBytes("Parabens, voce venceu!\n");
                        this.batedor = 1;//ganhador sempre e o primeiro a bater
                        servidorJogo.notificarVencedorJogadores();//notificando todos os jogadores que houve um vencedor
                    } else {
                        paraCliente.writeBytes("Calma voce ainda nao venceu!\n");
                        servidorJogo.incrementarPosicao();
                        //caso tenha chegado no ultimo jogador, voltar para o primeiro
                        if (this.posicao == servidorJogo.getQuantidadeJogadores()) {
                            servidorJogo.setJogadorDaVez(1);//setando para primeiro jogador
                        }
                        servidorJogo.notificarVezTodosJogadores(); //notificando jogador da ver para apertar enter
                    }

                } else {
                    doCliente.readLine(); //deixar jogador que não é da vez em espera
                }

            }

            while (servidorJogo.getStatus() == 2) {// status = finalização
                if (this.batedor == 0) {// para os jogadores que nao bateram
                    paraCliente.writeBytes("ATENCAO JOGADORES QUE NAO GANHARAM! Digitem /bater para nao perder! O ultimo sera o perdedor!\n");
                    String resposta = doCliente.readLine();//ler comando

                    if (resposta.equals("/bater")) {//caso o cliente digite /bater
                        this.batedor = 1;//setando variavel indicando que cliente ja bateu
                            servidorJogo.incrementaBatedor();// aumentando a quantidade de batedores no jogo
                            if (servidorJogo.getControleUltimoBatedor() == servidorJogo.getQuantidadeJogadores()) {//caso o cliente seja o ultimo batedor
                                paraCliente.writeBytes("Voce foi o ultimo a bater! Voce PERDEU o jogo!\n");//informando ao cliente que ele perdeu
                                //notificando todos os jogadores do perdedor
                                for (ServidorThread s : servidorJogo.getServidores()) {
                                    s.notificar("O jogador " + this.posicao + " de nome " + this.nome + " perdeu!\n");
                                }
                                servidorJogo.finalizar();//finalizando jogo ->status = -1
                            } else {
                                paraCliente.writeBytes("Voce bateu!\n");//informando cliente que ele bateu
                            }                        
                    }
                } else {
                    //caso o jogador ja tenha batido
                    paraCliente.writeBytes("Aguarde o fim do jogo!\n");
                    doCliente.readLine();
                }
            }
            conexao.close(); // fechando conexão
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
