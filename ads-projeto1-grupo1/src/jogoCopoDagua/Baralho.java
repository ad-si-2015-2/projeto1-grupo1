package jogoCopoDagua;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 *
 * @author Douglas França, Lucas Zenha
 */
public class Baralho {
    
    public int cartaOferecida;      // armazena a carta oferecida pelo baratalho
    public List<Integer> baralho = new ArrayList<>();  //armazena as cartas do baralho
    
    public Baralho(){
        
    }
    
    //Metodo gera carta valida para ser sacada do baralho
    public int novaCarta(){
        // Aleatoriedade ao sacar
        Random gerador = new Random();
        // Qual carta no baralho a ser sacada
        int index = gerador.nextInt(35);
        // Verificando se a carta ja nao foi sacada
        if(cartaValida(index)){
            cartaOferecida = baralho.get(index);
            baralho.set(index, 0);
        } else {
            novaCarta();
        }
        return this.cartaOferecida;        
    }
    
    // Metodo cria baralho com 4 cartas de mesmo numero. Cartas de 1 a 9
    public List<Integer> criarBaralho(){
        // Loop para populaçao do baralho
        for(int i=1;i<=9;i++){
            for(int j=0;j<=3;j++){
                baralho.add(i);
            }
        }
        return baralho;
    }
    
    // Metodo para conferir se a carta ja foi sacada (no caso 0)
    public boolean cartaValida(int index){
        return baralho.get(index) != 0;
    }
}
