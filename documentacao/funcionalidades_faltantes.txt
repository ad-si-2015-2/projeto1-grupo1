Aplicativo -> Jogo Copo D'agua

Funcionalidades que ainda faltam:

1 - Distribuir três cartas para cada participante no início do jogo - ok
2 - Criar variável para armazenar descarte - ok
3 - A cada jogada, mostrar o descarte para o participante, antes de permitir ele comprar
uma carta - ok
4 - Caso o jogador não queira a carta jogada ela vira descarte - ok
4 - Quando um participante ganhar, informar a todos os outros - ok
5 - O ultimo a digitar /bater perde o jogo - ok
6 - Enviar relatorio ao terminar o jogo - ok
